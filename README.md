# GRASEC-IoT 

## A Graph Dataset for Security Enforcement in IoT Networks
### Overview

The Graph-Based Dataset for IoT Network Attack Detection is a curated collection of data specifically designed for research and development in the field of cybersecurity, focusing on the detection of attacks in Internet of Things (IoT) networks. This graph-based dataset provides researchers, developers, and practitioners with a comprehensive resource to evaluate and benchmark various detection algorithms and systems in real-world IoT network environments.

## Key Features

- Graph Representation: Network traffic data represented as graphs, facilitating intuitive visualization and analysis.
- Attack Scenarios: Diverse attack scenarios, including DDoS attacks, HTTP Get/Post flood, TCP SYN flood, UDP flood, ICMP flood, brute force and port scanning.
- Realistic Environment: emulated IoT network environments reflecting real-world conditions and configurations.
- Anomaly Labels: Ground truth labels for anomalous network activity, enabling supervised learning approaches for attack detection.

## Dataset Description

The dataset consists of network traffic data captured from emulated IoT network environments, where various attack scenarios have been emulated. The network traffic data is represented in the form of graphs, capturing the interactions and relationships between different devices, services, and communication patterns within the IoT network. Each graph in the dataset represents a snapshot of network activity over a specific time period, enabling analysis of attack patterns and behaviors. The following figure presents the general netwok architecture.

<div align="center">
<img src="images/image-1.png" title="Overview of the IoT network architecture" alt="alt text" width="500"  />
<em>Overview of the IoT network architecture</em>
</div>

You find in this dataset the graph that contains all the attacks and the Pcap csv file per attack. The graph containing all attacks is the merge of attacks including normal traffic. 

To construct our dataset, we initially establish a test environment mirroring real-world networks and subsequently simulate diverse forms of attacks. Our testbed architecture comprises two primary components: the user network and the adversary network. Within the user network, we incorporate standard elements typical of IoT networks, including end-user devices executing various tasks to replicate the traffic patterns commonly encountered in such networks. Additionally, virtual machines emulate smart devices (IoT Devices), generating typical traffic associated with these devices. Furthermore, we integrate an onsite server furnishing services to network users, serving as the focal point for the attacks we execute on the network.

Conversely, the adversary network features a botnet comprising multiple zombie machines overseen by a singular bot-master machine functioning as a Command and Control (C&C) server. Through this central machine, we can orchestrate an array of botnet attacks directed at the user network. The following figure illustrates the architectural of our testbed.

<div align="center">
<img src="images/image-2.png" alt="alt text" width="800" title="The detialled network architecture" /> 
<em>The detialled network architecture</em>
</div>

For implementing this environment, we used the GNS3 tool. It is an open-source software for network emulation. It empowers users to design, configure, and test intricate network topologies within a virtual environment. 

## 1. Normal traffic generation
For normal traffic generation, we used multiple virtual machine that were supposed to mimic the behavior of real network users. It consisted of 3 Ubuntu VMs that served as users and another Ubuntu VM that played the role of a local server providing services such as web site hosting and file sharing using FTP. We also used an Ubuntu VM to run IoT-Flock, an IoT devices simulation tool. We used it to simulate the following devices: Light intensity sensor, Temperature sensor, Smoke sensor, Door lock, Fan speed controller.
## 2. Attack scenarios
As for the attack scenarios, we used a botnet composed of multiple VMs controlled by a Kali virtual machine. This botnet was able to launch a variety of network attacks on the local server described above. These attacks included the following: HTTP GET flood, HTTP POST flood, ICMP flood, TCP SYN flood, UDP flood, Port scanning and Brute force.
## 3. Data capture
To capture the network trafic in our environment, we used Wireshark tool to record the network data in pcap format and used a flow capture tool called CICFlowmeter.

### Packets capture
we use the Wireshark tool to record the network data in pcap format. They are available in the GRASEC-IoT gitlab \cite{grasec}. An exemple of features (the complete list of 83 features is in the pcap-csv file above):

<div align="center">
<img src="images/image-5.png" alt="alt text" width="800" />
</div>

### Flows capture
We use the CICFlowMeter tool to extract flows from Pcap files.

<div align="center">
<img src="images/image-6.png" alt="alt text" width="800" />
</div>
*The first packet determines the forward (source to destination) and backward (destination to source) directions. 


## Graph modeling 
A graph dataset proves to be particularly advantageous for network attack detection, as it facilitates the examination of relationships and interactions among the various entities within the network. There are two primary methods for modeling network activity using a graph.

In the first approach, nodes depict individual entities within the network such as servers, IoT devices, etc., while links represent interactions between these entities. Alternatively, the second method involves representing both network entities and flows as nodes. For instance, from a flow consisting of a source host (S) and a destination host (D), two undirected edges are created, as illustrated in the following Figure.

Representing flows as nodes in the graph aligns well with Graph Neural Network (GNN) algorithms, which primarily focus on nodes in the embedding (hidden state). The graphs comprising the dataset are accessible on GitLab in JSON format, facilitating ease of access and utilization for network analysis and attack detection purposes.

<div align="center">
<img src="images/Flow.png" alt="alt text" width="500" />
</div>

## Dataset Access

The GRASEC-IoT Dataset is available for download and exploration via this gitlab.

## Citation

If you use this dataset in your research or projects, please cite the following publication: D. Hamouche, R. Kadri, M.-L. Messai, H. Seba. A Graph Dataset for Security Enforcement in IoT Networks: GRASEC-IoT. In : 2024 20th International Conference on Distributed Computing in Smart Systems and the Internet of Things (DCOSS-IoT). IEEE, 2024. p. 1-8.  


## Authors and acknowledgment
Those who have contributed to the project: Djameleddine Hamouche, Mohamed Reda Kadri, Mohamed-Lamine Messai, Hamida Seba. 
This work is supported by the French National Research Agency (ANR) under grant ANR-20-CE39-0008.

<div align="left">
<img src="images/ANR-logo-2021-sigle.jpg" alt="alt text" width="120" />
<img src="images/logo_liris.png" alt="alt text" width="120" />
<img src="images/logo_eric.png" alt="alt text" width="120" />
</div>

## License
Creative Commons Attribution. CC BY 4.0 Deed Attribution 4.0 International.

<img src="images/image-3.png" alt="alt text" width="250" />

## Project status
Current 

